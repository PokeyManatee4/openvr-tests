#!/bin/zsh
set -e

vcvars='C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat'

cd "$(dirname $0)"
cd build-native

if [[ $1 == --cmd ]]; then
	exec cmd.exe /k $vcvars
fi

cmd.exe /k $vcvars '&' 'ninja' </dev/null
