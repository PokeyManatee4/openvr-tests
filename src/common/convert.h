//
// Created by znix on 27/06/2021.
//

#pragma once

#include <glm/mat4x4.hpp>
#include <openvr.h>

glm::mat4 vr_to_glm(const vr::HmdMatrix34_t &mat);
