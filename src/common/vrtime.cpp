//
// Created by znix on 04/07/2021.
//

#include "vrtime.h"

#include <time.h>

uint64_t get_current_time() {
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ts.tv_nsec + ts.tv_sec * 1000000000L;
}
