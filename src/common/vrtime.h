//
// Created by znix on 04/07/2021.
//

#pragma once

#include <inttypes.h>

/**
 * Get the current monotonic time in nanoseconds.
 */
uint64_t get_current_time();
