//
// Created by ZNix on 20/06/2021.
//

#pragma once

#include <openvr.h>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/matrix.hpp>

#define INTERNAL_TEST_CONCAT2(a, b) a##b
#define INTERNAL_TEST_CONCAT(a, b) INTERNAL_TEST_CONCAT2(a, b)

#define VRTS_TESTS_INTERNAL_AK2(file, line) file "-" #line
#define VRTS_TESTS_INTERNAL_AK1(file, line) VRTS_TESTS_INTERNAL_AK2(file, line)
#define TEST_KEY VRTS_TESTS_INTERNAL_AK1(__FILE__, __LINE__)

/**
 * A macro to indicate in a standardised way that a function is yet
 * to be tested. If we ever get 100% coverage we can remove this and
 * make sure everything still builds as a kind of verification.
 */
#define TODO_TEST(name)

/**
 * Tag a test as being part of a specific group. This is how we run some tests
 * every frame or on other events.
 */
#define TEST_RUN_GROUP(tag)

#define TEST(name) void INTERNAL_TEST_CONCAT(Test, TEST_TARGET)::Test##name()

typedef bool (*vrAssertHandler_t)(const char *key, bool cond, const char *msg);
extern vrAssertHandler_t vrAssertHandler;

struct RemoteHandControllerState {
	bool menu = false;
	bool select = false;
};

struct RemoteControlState {
	RemoteHandControllerState left, right;
};
extern RemoteControlState *remoteControlState;

template <typename T> std::string val_to_string(const T &value) { return std::to_string(value); }
template <> std::string val_to_string(const std::string &value);
template <> std::string val_to_string(const glm::vec3 &value);
template <> std::string val_to_string(const glm::vec4 &value);

/**
 * Assert that a given condition is true, or fail with the specified message.
 */
#define VR_ASSERT(cond)                                                                                                \
	do {                                                                                                               \
		if (!vrAssertHandler(TEST_KEY, cond, #cond))                                                                   \
			return;                                                                                                    \
	} while (0)

/**
 * Assert that two values are equal. The first value is considered 'good'.
 */
#define VR_ASSERT_EQ(goodVal, testVal)                                                                                 \
	do {                                                                                                               \
		auto testValEval = (testVal);                                                                                  \
		std::string msg = std::string(__func__) + ": Assert '" #testVal "' == " #goodVal " failed for " +              \
		                  val_to_string(testValEval);                                                                  \
		if (!vrAssertHandler(TEST_KEY, goodVal == testValEval, msg.c_str()))                                           \
			return;                                                                                                    \
	} while (0)

#define VR_ASSERT_EQ_FLOAT(goodVal, testVal)                                                                           \
	do {                                                                                                               \
		if (glm::abs(goodVal - testVal) > 0.0001)                                                                      \
			VR_ASSERT_EQ(goodVal, testVal);                                                                            \
	} while (0)
