#include "TestSystem.h"

#define TEST_TARGET System

#include "convert.h"

#include <map>

// See GetProjectionMatrix
static glm::mat4 compose_wrong_projection(float fLeft, float fRight, float fTop, float fBottom, float zNear,
                                          float zFar) {
	float idx = 1.0f / (fRight - fLeft);
	float idy = 1.0f / (fBottom - fTop);
	float idz = 1.0f / (zFar - zNear);
	float sx = fRight + fLeft;
	float sy = fBottom + fTop;

	glm::mat4 p;
	// clang-format off
	p[0][0] = 2*idx; p[0][1] = 0;     p[0][2] = sx*idx;    p[0][3] = 0;
	p[1][0] = 0;     p[1][1] = 2*idy; p[1][2] = sy*idy;    p[1][3] = 0;
	p[2][0] = 0;     p[2][1] = 0;     p[2][2] = -zFar*idz; p[2][3] = -zFar*zNear*idz;
	p[3][0] = 0;     p[3][1] = 0;     p[3][2] = -1.0f;     p[3][3] = 0;
	// clang-format on

	// We computed it transposed since I copied the code from the OpenVR wiki and didn't want to
	// fiddle with all the indices.
	// https://github.com/ValveSoftware/openvr/wiki/IVRSystem::GetProjectionRaw
	return glm::transpose(p);
}

TEST(GetRecommendedRenderTargetSize) {
	uint32_t width = 0;
	uint32_t height = 0;
	vr::VRSystem()->GetRecommendedRenderTargetSize(&width, &height);

	VR_ASSERT_EQ(2372, width);
	VR_ASSERT_EQ(1332, height);

	// As of SteamVR version 1623188486 these segfault on Linux, probably same on Windows
	// uint32_t width2, height2;
	// vr::VRSystem()->GetRecommendedRenderTargetSize(&width2, nullptr);
	// vr::VRSystem()->GetRecommendedRenderTargetSize(nullptr, &height2);
	// vr::VRSystem()->GetRecommendedRenderTargetSize(nullptr, nullptr);
	// VR_ASSERT_EQ(width, width2);
	// VR_ASSERT_EQ(height, height2);
}

TEST(GetProjectionMatrix) {
	float near = 123;
	float far = 456;

	for (int eye = 0; eye < 2; eye++) {
		vr::HmdMatrix44_t vrMatProjection = vr::VRSystem()->GetProjectionMatrix((vr::EVREye)eye, near, far);
		glm::mat4 vrProjection;
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				// Transpose, since openvr is row-major
				vrProjection[y][x] = vrMatProjection.m[x][y];
			}
		}

		// Compute the matrix based on GetProjectionRaw. Unfortunately there appears to be a bug in the
		// function they use to compute their projection matrix, as they fail to account for the near plane
		// in the Z value calculation: https://github.com/ValveSoftware/openvr/issues/1052
		// I doubt this will ever change, since it would be a very nasty trap when upgrading openvr versions.
		float left, right, up, down;
		vr::VRSystem()->GetProjectionRaw((vr::EVREye)eye, &left, &right, &up, &down);
		glm::mat4 ourProjection = compose_wrong_projection(left, right, up, down, near, far);

		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				// Ignore very small floating-point precision errors
				float delta = ourProjection[y][x] - vrProjection[y][x];
				if (glm::abs(delta) > 0.001)
					VR_ASSERT_EQ(ourProjection[y][x], vrProjection[y][x]);
			}
		}
	}

	// Invalid eye parameter - the resulting view matrix must be the same as the left eye.
	vr::HmdMatrix44_t leftEye = vr::VRSystem()->GetProjectionMatrix(vr::Eye_Left, near, far);
	for (int eye = 2; eye < 5; eye++) {
		vr::HmdMatrix44_t invalidEye = vr::VRSystem()->GetProjectionMatrix((vr::EVREye)2, near, far);
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				VR_ASSERT_EQ_FLOAT(leftEye.m[y][x], invalidEye.m[y][x]);
			}
		}
	}
}
TODO_TEST(GetProjectionRaw)
TODO_TEST(ComputeDistortion)
TEST(GetEyeToHeadTransform) {
	// Hardcoded as 63mm for now, obviously may change in future Monado versions
	// https://gitlab.freedesktop.org/monado/monado/-/blob/5838d737b3b57947b29fb1538a23deb0d8627e94/src/xrt/state_trackers/steamvr_drv/ovrd_driver.cpp#L755
	float ipd = 0.063;

	for (int eye = 0; eye < 10; eye++) {
		vr::HmdMatrix34_t mat = vr::VRSystem()->GetEyeToHeadTransform((vr::EVREye)eye);

		glm::mat4 ourMat = glm::identity<glm::mat4>();
		ourMat[3][0] = ipd / 2;
		if (eye == vr::Eye_Left || eye >= 2)
			ourMat[3][0] *= -1;

		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 4; x++) {
				// Ignore very small floating-point precision errors
				// As usual, transpose at this point
				float delta = ourMat[x][y] - mat.m[y][x];
				if (glm::abs(delta) > 0.001)
					VR_ASSERT_EQ(ourMat[x][y], mat.m[y][x]);
			}
		}
	}
}
TODO_TEST(GetTimeSinceLastVsync)
TODO_TEST(GetD3D9AdapterIndex)
TODO_TEST(GetDXGIOutputInfo)
TODO_TEST(GetOutputDevice)
TODO_TEST(IsDisplayOnDesktop)
TODO_TEST(SetDisplayVisibility)
TODO_TEST(GetDeviceToAbsoluteTrackingPose)
TODO_TEST(GetSeatedZeroPoseToStandingAbsoluteTrackingPose)
TODO_TEST(GetRawZeroPoseToStandingAbsoluteTrackingPose)
TODO_TEST(GetSortedTrackedDeviceIndicesOfClass)
TODO_TEST(GetTrackedDeviceActivityLevel)
TEST(ApplyTransform) {
	vr::TrackedDevicePose_t in = {};
	in.bPoseIsValid = true;
	for (int x = 0; x < 3; x++) {
		for (int y = 0; y < 4; y++) {
			in.mDeviceToAbsoluteTracking.m[x][y] = y + x * 0.1f;
		}
		in.vAngularVelocity.v[x] = x * 0.2f;
		in.vVelocity.v[x] = 1 - x * 0.2f;
	}
	in.bDeviceIsConnected = false;
	in.eTrackingResult = vr::TrackingResult_Calibrating_OutOfRange;

	vr::HmdMatrix34_t transform = {};
	transform.m[0][0] = 1;
	transform.m[1][2] = 1;
	transform.m[2][1] = 1;

	transform.m[0][3] = 123;
	transform.m[1][3] = 456;
	transform.m[2][3] = 789;

	vr::TrackedDevicePose_t out = {};
	vr::VRSystem()->ApplyTransform(&out, &in, &transform);

	VR_ASSERT_EQ(in.bDeviceIsConnected, out.bDeviceIsConnected);
	VR_ASSERT_EQ(in.bPoseIsValid, out.bPoseIsValid);
	VR_ASSERT_EQ(in.eTrackingResult, out.eTrackingResult);

	glm::mat4 xform = vr_to_glm(transform);
	// Note this multiplication is flipped, since openvr is all row-major
	// Note quite sure *why* this is the case even after we've transposed both our input
	//  matrices, but it matches anyway.
	glm::mat4 requiredOut = vr_to_glm(in.mDeviceToAbsoluteTracking) * xform;

	for (int x = 0; x < 4; x++) {
		for (int y = 0; y < 3; y++) {
			// Transpose the output matrix while reading from it
			VR_ASSERT_EQ(requiredOut[x][y], out.mDeviceToAbsoluteTracking.m[y][x]);
		}
		in.vAngularVelocity.v[x] = x * 0.2f;
		in.vVelocity.v[x] = 1 - x * 0.2f;
	}
}
TEST(GetTrackedDeviceIndexForControllerRole) {
	std::map<vr::ETrackedControllerRole, int> deviceForRole;

	// Check invalid device IDs
	VR_ASSERT_EQ(vr::TrackedControllerRole_Invalid, vr::VRSystem()->GetControllerRoleForTrackedDeviceIndex(999));

	// Check invalid controller roles
	VR_ASSERT_EQ(-1, vr::VRSystem()->GetTrackedDeviceIndexForControllerRole((vr::ETrackedControllerRole)123));

	for (int dev = 0; dev < vr::k_unMaxTrackedDeviceCount; dev++) {
		vr::ETrackedControllerRole role = vr::VRSystem()->GetControllerRoleForTrackedDeviceIndex(dev);
		if (!deviceForRole.count(role) && role != vr::TrackedControllerRole_Invalid)
			deviceForRole[role] = dev;
	}

	for (const auto &pair : deviceForRole) {
		int dev = vr::VRSystem()->GetTrackedDeviceIndexForControllerRole(pair.first);
		VR_ASSERT_EQ(pair.second, dev);
	}
}
TODO_TEST(GetControllerRoleForTrackedDeviceIndex)
TODO_TEST(GetTrackedDeviceClass)
TODO_TEST(IsTrackedDeviceConnected)
TODO_TEST(GetBoolTrackedDeviceProperty)
TODO_TEST(GetFloatTrackedDeviceProperty)
TODO_TEST(GetInt32TrackedDeviceProperty)
TODO_TEST(GetUint64TrackedDeviceProperty)
TODO_TEST(GetMatrix34TrackedDeviceProperty)
TODO_TEST(GetArrayTrackedDeviceProperty)
TODO_TEST(GetStringTrackedDeviceProperty)
TODO_TEST(GetPropErrorNameFromEnum)
TODO_TEST(PollNextEvent)
TODO_TEST(PollNextEventWithPose)
TODO_TEST(GetEventTypeNameFromEnum)
TODO_TEST(GetHiddenAreaMesh)
TODO_TEST(GetControllerState)
TODO_TEST(GetControllerStateWithPose)
TODO_TEST(TriggerHapticPulse)
TEST(GetButtonIdNameFromEnum) {
	// Go through all the valid button values and one more
	for (int id = 0; id < vr::k_EButton_Max + 1; id++) {
		const char *name = vr::VRSystem()->GetButtonIdNameFromEnum((vr::EVRButtonId)id);
		std::string correct;

#define BTN_NAME(id_name)                                                                                              \
	case vr::id_name:                                                                                                  \
		correct = #id_name;                                                                                            \
		break;

		switch (id) {
			BTN_NAME(k_EButton_System)
			BTN_NAME(k_EButton_ApplicationMenu)
			BTN_NAME(k_EButton_Grip)
			BTN_NAME(k_EButton_DPad_Left)
			BTN_NAME(k_EButton_DPad_Up)
			BTN_NAME(k_EButton_DPad_Right)
			BTN_NAME(k_EButton_DPad_Down)
			BTN_NAME(k_EButton_A)

			BTN_NAME(k_EButton_ProximitySensor)

			BTN_NAME(k_EButton_Axis0)
			BTN_NAME(k_EButton_Axis1)
			BTN_NAME(k_EButton_Axis2)
			BTN_NAME(k_EButton_Axis3)
			BTN_NAME(k_EButton_Axis4)
		default:
			char tmp[1024];
			sprintf(tmp, "Unknown EVRButtonId (%d)", id);
			correct = tmp;
			break;
		}

#undef BTN_NAME

		VR_ASSERT_EQ(correct, std::string(name));
	}
}
TEST(GetControllerAxisTypeNameFromEnum) {
	for (int id = 0; id < 50; id++) {
		const char *name = vr::VRSystem()->GetControllerAxisTypeNameFromEnum((vr::EVRControllerAxisType)id);
		std::string correct;

		switch (id) {
		case vr::k_eControllerAxis_None:
			correct = "k_eControllerAxis_None";
			break;
		case vr::k_eControllerAxis_TrackPad:
			correct = "k_eControllerAxis_TrackPad";
			break;
		case vr::k_eControllerAxis_Joystick:
			correct = "k_eControllerAxis_Joystick";
			break;
		case vr::k_eControllerAxis_Trigger:
			correct = "k_eControllerAxis_Trigger";
			break;
		default:
			char tmp[1024];
			sprintf(tmp, "Unknown EVRControllerAxisType (%d)", id);
			correct = tmp;
			break;
		}

		VR_ASSERT_EQ(correct, std::string(name));
	}
}
TODO_TEST(IsInputAvailable)
TODO_TEST(IsSteamVRDrawingControllers)
TODO_TEST(ShouldApplicationPause)
TODO_TEST(ShouldApplicationReduceRenderingWork)
TODO_TEST(PerformFirmwareUpdate)
TODO_TEST(AcknowledgeQuit_Exiting)
TODO_TEST(GetAppContainerFilePaths)
TODO_TEST(GetRuntimeVersion)
