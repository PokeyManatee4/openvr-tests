#include "TestInput.h"

#include <unistd.h>

#define TEST_TARGET Input

#define vri (vr::VRInput())

TEST(SetActionManifestPath) {
	char buf[1024];
	std::string path = getcwd(buf, sizeof(buf));
	path += "/../assets/actions.json";
	VR_ASSERT_EQ(0, vri->SetActionManifestPath(path.c_str()));
}
TEST(GetActionSetHandle) {
	// It seems you can pass in any old string and it will assign it an ID
	// By taking multiple different strings (thus ash3) we can check it's not passing back some
	// kind of dummy null value.
	vr::VRActionSetHandle_t ash1 = 1, ash2 = 2, ash3 = 3;
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("blahblahblah", &ash1));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("blahblahblah", &ash2));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("blahblahblah2", &ash3));
	VR_ASSERT_EQ(true, ash1 != 1);
	VR_ASSERT_EQ(true, ash2 != 2);
	VR_ASSERT_EQ(true, ash3 != 3);
	VR_ASSERT_EQ(ash1, ash2);
	VR_ASSERT_EQ(true, ash1 != ash3);

	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("/actions/main", &m_mainSet));
	VR_ASSERT_EQ(true, m_mainSet != vr::k_ulInvalidActionSetHandle);
}
TEST(GetActionHandle) {
	vr::VRActionHandle_t ah1 = 1, ah2 = 2, ah3 = 3;
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("testing", &ah1));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("testing", &ah2));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("testing2", &ah3));
	VR_ASSERT_EQ(true, ah1 != 1);
	VR_ASSERT_EQ(true, ah2 != 2);
	VR_ASSERT_EQ(true, ah3 != 3);
	VR_ASSERT_EQ(ah1, ah2);
	VR_ASSERT_EQ(true, ah1 != ah3);

	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("/actions/main/in/OpenInventory", &m_btn1));
	VR_ASSERT_EQ(true, m_btn1 != vr::k_ulInvalidActionSetHandle);
}
TODO_TEST(GetInputSourceHandle)
TEST_RUN_GROUP(PerFrame)
TEST(UpdateActionState) {
	vr::VRActiveActionSet_t as = {};
	as.ulActionSet = m_mainSet;
	VR_ASSERT_EQ(vr::VRInputError_None, vri->UpdateActionState(&as, sizeof(as), 1));
}
TODO_TEST(GetDigitalActionData)
TODO_TEST(GetAnalogActionData)
TODO_TEST(GetPoseActionDataRelativeToNow)
TODO_TEST(GetPoseActionDataForNextFrame)
TODO_TEST(GetSkeletalActionData)
TODO_TEST(GetDominantHand)
TODO_TEST(SetDominantHand)
TODO_TEST(GetBoneCount)
TODO_TEST(GetBoneHierarchy)
TODO_TEST(GetBoneName)
TODO_TEST(GetSkeletalReferenceTransforms)
TODO_TEST(GetSkeletalTrackingLevel)
TODO_TEST(GetSkeletalBoneData)
TODO_TEST(GetSkeletalSummaryData)
TODO_TEST(GetSkeletalBoneDataCompressed)
TODO_TEST(DecompressSkeletalBoneData)
TODO_TEST(TriggerHapticVibrationAction)
TODO_TEST(GetActionOrigins)
TODO_TEST(GetOriginLocalizedName)
TODO_TEST(GetOriginTrackedDeviceInfo)
TODO_TEST(GetActionBindingInfo)
TODO_TEST(ShowActionOrigins)
TODO_TEST(ShowBindingsForActionSet)
TODO_TEST(GetComponentStateForBinding)
TODO_TEST(IsUsingLegacyInput)
TODO_TEST(OpenBindingUI)
TODO_TEST(GetBindingVariant)
