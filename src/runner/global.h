//
// Created by ZNix on 13/06/2021.
//

#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <openvr.h>

#include <vrtime.h>

#include <assert.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <optional>

typedef GLuint VBO;
typedef GLuint VAO;
typedef GLuint FBO;
typedef GLuint ShaderSrc;
typedef GLint UniformLoc;
typedef GLuint Texture;
