//
// Created by ZNix on 20/06/2021.
//

#pragma once

#include "global.h"

#define VRTS_INTERNAL_AK2(file, line) file "-" #line
#define VRTS_INTERNAL_AK1(file, line) VRTS_INTERNAL_AK2(file, line)
#define VRTS_AUTO_KEY VRTS_INTERNAL_AK1(__FILE__, __LINE__)

#define ASSERT_SVR_ZERO(expr) assert_svr_zero(VRTS_AUTO_KEY, #expr, expr)

/**
 * Assert that a value is zero, and fail with an error message if not.
 *
 * @return The err value passed in.
 */
int assert_svr_zero(const char *id_key, const char *msg, int err);

bool vr_assert(const char *id_key, bool cond, const char *msg);
