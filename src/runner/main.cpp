#include "global.h"

#include "asserts.h"
#include "render.h"

#include "interfaces/TestInput.h"
#include "interfaces/TestSystem.h"
#include "monado_remote.h"

#include <stdio.h>

#include <GLFW/glfw3.h>

static void cb_glfw_err(int type, const char *msg) { fprintf(stderr, "[GLFW %d] %s", type, msg); }

vrAssertHandler_t vrAssertHandler = vr_assert;
RemoteControlState *remoteControlState = nullptr;

int main(int argc, char **argv) {
	GLFWwindow *window = nullptr;
	MonadoConnection *conn = nullptr;
	uint64_t startTime = get_current_time();

	TestSystem ts;
	TestInput ti;

	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialise glfw.\n");
		return 1;
	}

	// From this point on we shouldn't return, and instead goto to err

	glfwSetErrorCallback(cb_glfw_err);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(480, 360, "OpenVR Unofficial Test suite", nullptr, nullptr);

	if (!window) {
		fprintf(stderr, "Failed to create window\n");
		goto err;
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	if (!gladLoadGL()) {
		fprintf(stderr, "Failed to load GLAD\n");
		goto err;
	}

	// Setup OpenVR
	vr::EVRInitError vrInitErr;
	vr::VR_Init(&vrInitErr, vr::VRApplication_Scene);
	if (vrInitErr != vr::VRInitError_None) {
		const char *msg = vr::VR_GetVRInitErrorAsEnglishDescription(vrInitErr);
		fprintf(stderr, "Failed to initialise OpenVR: %s\n", msg);
		goto err;
	}

	if (do_render_setup()) {
		fprintf(stderr, "Failed to initialise renderer\n");
		goto err;
	}

	// Connect to Monado and take control of our virtual input device
	if (MonadoConnection::connect_to_monado(&conn, "localhost", 4242)) {
		fprintf(stderr, "Failed to connect to Monado remote port\n");
		goto err;
	}

	// Run our tests
	ts.RunStartup();
	ti.RunStartup();

	remoteControlState = new RemoteControlState;

	while (!glfwWindowShouldClose(window)) {
		uint64_t currentTimeNs = get_current_time() - startTime;
		float currentTime = currentTimeNs / 1000000000.0f;

		do_render_draw(window);

		glfwSwapBuffers(window);
		glfwPollEvents();

		remoteControlState->left.menu = (int)(currentTime / 2) % 2 == 0;
		remoteControlState->left.select = (int)(currentTime / 2.5) % 2 == 0;
		conn->set_control_state(vr::Eye_Left, true, remoteControlState->left.select, remoteControlState->left.menu);

		glm::vec3 offset = glm::vec3(0, sin(currentTime) / 10, 0);
		conn->set_hmd_pose(offset + glm::vec3(0, 1.5, 0), glm::quat(1, 0, 0, 0));
		conn->set_hand_pose(offset + glm::vec3(0.15, 1.5, -0.3), glm::quat(1, 0, 0, 0), vr::Eye_Right);
		conn->set_hand_pose(offset + glm::vec3(-0.5, 1.5, -0.3), glm::quat(1, 0, 0, 0), vr::Eye_Left);
		conn->send_update();

		ts.RunPerFrame();
		ti.RunPerFrame();
	}

	printf("Hello, World!\n");

	//

err:
	vr::VR_Shutdown();

	if (window)
		glfwDestroyWindow(window);
	glfwTerminate();
}
