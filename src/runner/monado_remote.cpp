// Copied from monado remote driver r_hub.c
// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include "global.h"

#include "monado_remote.h"
#include "monado_remote_def.h"

#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// FIXME do this properly
#define U_LOG_E(...) fprintf(stderr, __VA_ARGS__)

static xrt_vec3 vec3_to_monado(glm::vec3 v) { return xrt_vec3{v.x, v.y, v.z}; }
static xrt_quat quat_to_monado(glm::quat q) { return xrt_quat{q.x, q.y, q.z, q.w}; }
xrt_pose MonadoConnection::GlmPose::to_monado() const {
	return xrt_pose{quat_to_monado(rotation), vec3_to_monado(position)};
}

void MonadoConnection::GlmPose::apply_inputs(r_remote_controller_data &data) const {
	data.active = active;
	data.select = select;
	data.menu = menu;
}

int MonadoConnection::connect_to_monado(MonadoConnection **out, const char *ip_addr, uint16_t port) {
	*out = nullptr;

	struct sockaddr_in addr = {0};
	int ret;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);

	ret = inet_pton(AF_INET, ip_addr, &addr.sin_addr);
	if (ret < 0) {
		U_LOG_E("socket failed: %i", ret);
		return ret;
	}

	ret = socket(AF_INET, SOCK_STREAM, 0);
	if (ret < 0) {
		U_LOG_E("socket failed: %i", ret);
		return ret;
	}

	int fd = ret;

	ret = connect(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (ret < 0) {
		U_LOG_E("connect failed: %i", ret);
		close(fd);
		return ret;
	}

	int flags = 1;
	ret = setsockopt(fd, SOL_TCP, TCP_NODELAY, (void *)&flags, sizeof(flags));
	if (ret < 0) {
		U_LOG_E("setsockopt failed: %i", ret);
		close(fd);
		return ret;
	}

	MonadoConnection *&conn = *out;
	conn = new MonadoConnection();
	conn->m_conn_fd = fd;

	struct r_remote_data data;
	conn->read_data(&data);
	conn->read_data(&data);

	return 0;
}

int MonadoConnection::read_data(struct r_remote_data *data) const {
	const size_t size = sizeof(*data);
	size_t current = 0;

	while (current < size) {
		void *ptr = (uint8_t *)data + current;

		ssize_t ret = read(m_conn_fd, ptr, size - current);
		if (ret < 0) {
			return ret;
		}

		if (ret > 0) {
			current += (size_t)ret;
		} else {
			abort(); // FIXME
			// U_LOG_I("Disconnected!");
			return -1;
		}
	}

	return 0;
}

int MonadoConnection::write_data(const struct r_remote_data *data) const {
	const size_t size = sizeof(*data);
	size_t current = 0;

	while (current < size) {
		const void *ptr = (const uint8_t *)data + current;

		ssize_t ret = write(m_conn_fd, ptr, size - current);
		if (ret < 0) {
			return ret;
		}

		if (ret > 0) {
			current += (size_t)ret;
		} else {
			abort(); // FIXME
			// U_LOG_I("Disconnected!");
			return -1;
		}
	}

	return 0;
}

void MonadoConnection::set_hmd_pose(glm::vec3 pos, glm::quat rot) { m_hmd_pose = GlmPose{pos, rot}; }

void MonadoConnection::set_hand_pose(glm::vec3 pos, glm::quat rot, vr::EVREye eye) {
	m_hand_poses[eye].position = pos;
	m_hand_poses[eye].rotation = rot;
}

void MonadoConnection::send_update() {
	struct r_remote_data data;
	memset(&data, 0, sizeof(data));

	data.header = R_HEADER_VALUE; // Not used, but leave it there for future-proofing
	data.hmd.pose = m_hmd_pose.to_monado();

	data.left.pose = m_hand_poses[vr::Eye_Left].to_monado();
	data.right.pose = m_hand_poses[vr::Eye_Right].to_monado();

	m_hand_poses[vr::Eye_Left].apply_inputs(data.left);
	m_hand_poses[vr::Eye_Right].apply_inputs(data.right);

	int result = write_data(&data);
	if (result) {
		fprintf(stderr, "Failed to write HMD update data: %d\n", result);
		abort();
	}
}

void MonadoConnection::set_control_state(vr::EVREye side, bool active, bool select, bool menu) {
	GlmPose &pose = m_hand_poses[side];
	pose.active = active;
	pose.select = select;
	pose.menu = menu;
}
