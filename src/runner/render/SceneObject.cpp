//
// Created by znix on 23/06/2021.
//

#include "global.h"

#include "SceneObject.h"

SceneObject::SceneObject(int shader) : shader(shader) { m_shaderVertMult = glGetUniformLocation(shader, "obj_to_cam"); }

void SceneObject::draw(glm::mat4 projection) { draw_at(projection, transform); }

void SceneObject::draw_at(glm::mat4 projection, glm::mat4 customTransform) {
	glBindVertexArray(m_model->get_or_build_vao());
	glUseProgram(shader);
	glUniformMatrix4fv(m_shaderVertMult, 1, false, glm::value_ptr(projection * customTransform));
	glDrawArrays(GL_TRIANGLES, 0, m_model->tris.size() * 3);
}
