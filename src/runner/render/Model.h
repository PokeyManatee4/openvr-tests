//
// Created by znix on 23/06/2021.
//

#pragma once

#include <vector>

struct ModelVert {
	glm::vec3 pos;
	glm::vec3 norm;
};

struct ModelTri {
	int a, b, c;
};

class Model {
  public:
	explicit Model();
	Model(const Model &) = delete;
	Model &operator=(const Model &other) = delete;

	VAO get_or_build_vao();

	std::vector<ModelVert> verts;
	std::vector<ModelTri> tris;

  private:
	std::optional<VAO> m_cachedVao;
};
