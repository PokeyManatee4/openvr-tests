//
// Created by znix on 23/06/2021.
//

#include "global.h"

#include "Model.h"
#include "render.h"

Model::Model() {}

VAO Model::get_or_build_vao() {
	if (m_cachedVao)
		return m_cachedVao.value();

	std::vector<float> vertData;
	vertData.resize(verts.size() * 6);
	for (int i = 0; i < verts.size(); i++) {
		const ModelVert &vert = verts.at(i);
		vertData.at(i * 6 + 0) = vert.pos.x;
		vertData.at(i * 6 + 1) = vert.pos.y;
		vertData.at(i * 6 + 2) = vert.pos.z;
		vertData.at(i * 6 + 3) = vert.norm.x;
		vertData.at(i * 6 + 4) = vert.norm.y;
		vertData.at(i * 6 + 5) = vert.norm.z;
	}
	VBO triangle = build_vbo(sizeof(float) * vertData.size(), vertData.data());

	VAO vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, triangle);
	int stride = sizeof(ModelVert);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void *)0);                   // pos
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void *)(sizeof(float) * 3)); // norm
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	// Produce the EBO
	// int indicies[] = {1, 2, 3};
	// GLuint ebo = 0;
	// glGenBuffers(1, &ebo);
	// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	// glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicies), indicies, GL_STATIC_DRAW);

	glBindVertexArray(0);
	m_cachedVao = vao;
	return vao;
}
