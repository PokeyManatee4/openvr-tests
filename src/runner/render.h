//
// Created by ZNix on 09/06/2021.
//

#pragma once

#include <GLFW/glfw3.h>
int do_render_setup();

void do_render_draw(GLFWwindow *window);

VBO build_vbo(int size, const void *data);
