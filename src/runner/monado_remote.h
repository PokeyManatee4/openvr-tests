//
// Created by znix on 22/06/2021.
//

#pragma once

#include <stdint.h>

class MonadoConnection {
  public:
	static int connect_to_monado(MonadoConnection **out, const char *ip_addr, uint16_t port);

	void set_hmd_pose(glm::vec3 pos, glm::quat rot);
	void set_hand_pose(glm::vec3 pos, glm::quat rot, vr::EVREye eye);
	void send_update();

	void set_control_state(vr::EVREye side, bool active, bool select, bool menu);

  private:
	int m_conn_fd = -1;

	explicit MonadoConnection() {}

	int read_data(struct r_remote_data *data) const;
	int write_data(const struct r_remote_data *data) const;

	struct GlmPose {
		glm::vec3 position;
		glm::quat rotation = glm::quat(1, 0, 0, 0);

		struct xrt_pose to_monado() const;
		void apply_inputs(struct r_remote_controller_data &data) const;

		bool active = false, select = false, menu = false;
	};

	GlmPose m_hmd_pose;
	GlmPose m_hand_poses[2]; // Indexed by vr::EVREye
};
