// Copied from monado remote driver r_interface.h
// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#pragma once

#include <stdint.h>

/*!
 * A quaternion with single floats.
 *
 * @ingroup xrt_iface math
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct xrt_quat {
	float x;
	float y;
	float z;
	float w;
};

/*!
 * A 3 element vector with single floats.
 *
 * @ingroup xrt_iface math
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct xrt_vec3 {
	float x;
	float y;
	float z;
};

/*!
 * A pose composed of a position and orientation.
 *
 * @see xrt_qaut
 * @see xrt_vec3
 * @ingroup xrt_iface math
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct xrt_pose {
	struct xrt_quat orientation;
	struct xrt_vec3 position;
};

/*!
 * Header value to be set in the packet.
 *
 * @ingroup drv_remote
 */
#define R_HEADER_VALUE (*(uint64_t *)"mndrmt1\0")

/*!
 * Data per controller.
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct r_remote_controller_data {
	struct xrt_pose pose;
	struct xrt_vec3 linear_velocity;
	struct xrt_vec3 angular_velocity;

	float hand_curl[5];

	bool active;
	bool select;
	bool menu;
	bool _pad;
};

/*!
 * Remote data sent from the debugger to the hub.
 *
 * @ingroup drv_remote
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct r_remote_data {
	uint64_t header;

	struct {
		struct xrt_pose pose;
	} hmd;

	struct r_remote_controller_data left, right;
};
