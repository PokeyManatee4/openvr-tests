//
// Created by ZNix on 09/06/2021.
//

#include "global.h"

#include "asserts.h"
#include "render.h"
#include "render/Model.h"
#include "render/SceneObject.h"
#include "render/obj_loader.h"
#include "shaders.h"

#include <GLFW/glfw3.h>

static void build_eye(Texture *colour, FBO *fbo);

VBO build_vbo(int size, const void *data) {
	VBO vbo = 0;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	return vbo;
}

static ShaderSrc compile_shader(const char *name, GLenum type, const char *src) {
	ShaderSrc vert = glCreateShader(type);
	glShaderSource(vert, 1, &src, nullptr);
	glCompileShader(vert);

	int status;
	glGetShaderiv(vert, GL_COMPILE_STATUS, &status);
	if (!status) {
		char msg[512];
		memset(msg, 0, sizeof(msg));
		glGetShaderInfoLog(vert, sizeof(msg) - 1, nullptr, msg);
		fprintf(stderr, "Failed to build %s shader: %s", name, msg);
		abort();
	}

	return vert;
}

static VBO triangle = 0;
static Texture eyeColour = 0;
static FBO eyeFbo = 0;
static SceneObject *controllerObj;

int do_render_setup() {
	float distance = -1;
	float verts[] = {// First triangle
	                 0.0f, 0.0f, distance, 1.0f, 0.0f, distance, 1.1f, 1.1f, distance,

	                 // Second triangle in the distance
	                 0.0f, 0.0f, distance * 2, 1.0f, 0.0f, distance * 2, 1.1f, 1.1f, distance * 2};
	triangle = build_vbo(sizeof(verts), verts);

	// Compile the shaders
	ShaderSrc vert = compile_shader("vert", GL_VERTEX_SHADER, get_vert_shader_src());
	ShaderSrc frag = compile_shader("frag", GL_FRAGMENT_SHADER, get_frag_shader_src());

	GLuint programme = glCreateProgram();
	glAttachShader(programme, vert);
	glAttachShader(programme, frag);
	glLinkProgram(programme);

	int status;
	glGetProgramiv(programme, GL_LINK_STATUS, &status);
	if (!status) {
		char msg[512];
		memset(msg, 0, sizeof(msg));
		glGetProgramInfoLog(programme, sizeof(msg) - 1, nullptr, msg);
		fprintf(stderr, "Failed to link shader: %s", msg);
		abort();
	}
	glUseProgram(programme);

	glDeleteShader(vert);
	glDeleteShader(frag);

	// Build and configure our VAO
	VAO vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, triangle);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);

	// Create the framebuffers
	build_eye(&eyeColour, &eyeFbo);

	Model *controllerModel = nullptr;
	load_obj(&controllerModel, "../assets/controller.obj");
	assert(controllerModel);
	controllerObj = new SceneObject(programme);
	controllerObj->m_model = std::shared_ptr<Model>(controllerModel);

	return 0;
}

static void build_eye(Texture *colour, FBO *fbo) {
	*colour = 0;
	*fbo = 0;

	uint32_t width = 1000;
	uint32_t height = 1000;
	vr::VRSystem()->GetRecommendedRenderTargetSize(&width, &height);

	FBO eye;
	glGenFramebuffers(1, &eye);
	glBindFramebuffer(GL_FRAMEBUFFER, eye);

	Texture tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

	Texture depth;
	glGenTextures(1, &depth);
	glBindTexture(GL_TEXTURE_2D, depth);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	*fbo = eye;
	*colour = tex;
}

static void do_render_draw_once(vr::EVREye eye) {
	float c = glfwGetTime();
	c -= (int)c;
	glClearColor(0.0F, 1.0F, c, 1.0F);
	glClear(GL_COLOR_BUFFER_BIT);

	glm::mat4 poseMats[vr::k_unMaxTrackedDeviceCount];
	vr::TrackedDevicePose_t poses[vr::k_unMaxTrackedDeviceCount];
	ASSERT_SVR_ZERO(vr::VRCompositor()->GetLastPoses(poses, vr::k_unMaxTrackedDeviceCount, nullptr, 0));
	for (int i = 0; i < vr::k_unMaxTrackedDeviceCount; i++) {
		glm::mat4 &mat = poseMats[i];
		mat = glm::identity<glm::mat4>();
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 4; y++) {
				mat[y][x] = poses[i].mDeviceToAbsoluteTracking.m[x][y];
			}
		}
	}

	float fovLeft, fovRight, fovTop, fovBottom; // NOLINT(readability-isolate-declaration)
	vr::VRSystem()->GetProjectionRaw(eye, &fovLeft, &fovRight, &fovTop, &fovBottom);
	glm::mat4 projection = glm::frustum<float>(fovLeft, fovRight, fovBottom, fovTop, 0.1, 10);

	glm::mat4 camTransform = poseMats[0];
	glm::mat4 toCamSpace = projection * glm::affineInverse(camTransform);

	for (int i = 1; i < vr::k_unMaxTrackedDeviceCount; i++) {
		if (!poses[i].bPoseIsValid)
			continue;

		controllerObj->draw_at(toCamSpace, poseMats[i]);
	}
}

void do_render_draw(GLFWwindow *window) {
	vr::Texture_t vrTex = {};
	vrTex.eColorSpace = vr::ColorSpace_Gamma;
	vrTex.eType = vr::TextureType_OpenGL;
	vrTex.handle = (void *)(intptr_t)eyeColour;

	vr::TrackedDevicePose_t trackedDevicePose[vr::k_unMaxTrackedDeviceCount];
	ASSERT_SVR_ZERO(vr::VRCompositor()->WaitGetPoses(trackedDevicePose, vr::k_unMaxTrackedDeviceCount, nullptr, 0));

	// Render desktop view
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	do_render_draw_once(vr::Eye_Left);

	// Render VR views
	uint32_t vrWidth, vrHeight;
	vr::VRSystem()->GetRecommendedRenderTargetSize(&vrWidth, &vrHeight);
	glViewport(0, 0, vrWidth, vrHeight);

	glBindFramebuffer(GL_FRAMEBUFFER, eyeFbo);
	do_render_draw_once(vr::Eye_Left);
	glFlush();
	ASSERT_SVR_ZERO(vr::VRCompositor()->Submit(vr::Eye_Left, &vrTex));

	glBindFramebuffer(GL_FRAMEBUFFER, eyeFbo);
	do_render_draw_once(vr::Eye_Right);
	glFlush();
	ASSERT_SVR_ZERO(vr::VRCompositor()->Submit(vr::Eye_Right, &vrTex));
}
