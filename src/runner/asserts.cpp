//
// Created by ZNix on 20/06/2021.
//

#include "asserts.h"

#include <map>
#include <set>
#include <string>

static std::set<std::string> hitAsserts;

int assert_svr_zero(const char *id_key, const char *msg, int err) {
	if (!err)
		return err;

	if (hitAsserts.count(id_key))
		return err;

	fprintf(stderr, "Assertion failed: %d != 0 %s\n", err, msg);
	hitAsserts.insert(id_key);

	return err;
}

bool vr_assert(const char *id_key, bool cond, const char *msg) {
	if (cond)
		return cond;

	if (hitAsserts.count(id_key))
		return cond;

	fprintf(stderr, "Assertion failed: %s\n", msg);
	hitAsserts.insert(id_key);

	return cond;
}
