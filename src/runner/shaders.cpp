//
// Created by ZNix on 13/06/2021.
//

#include "shaders.h"

const char *get_vert_shader_src() {
	return R"(#version 410 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 in_norm;

out vec3 norm;

uniform mat4 obj_to_cam;

void main() {
    norm = in_norm;
    gl_Position = obj_to_cam * vec4(pos, 1.0f);
}
)";
}
const char *get_frag_shader_src() {
	return R"(#version 410 core
out vec4 FragColour;
in vec3 norm;

uniform vec3 light_dir = normalize(vec3(0.5, 0.5, 0.5));

void main() {
    float diffuse_int = dot(norm, light_dir);

    FragColour = vec4(0.5f, 0.5f, 0.0f, 1.0f) * (diffuse_int + 0.2);
})";
}
